package iter

import (
	"os"
	"testing"
	"strings"
	"fmt"

	"gitlab.com/firelizzard/go-iter/alt/iterate"
)

type KVP = iterate.KVP

func expect[T any](t *testing.T, it iterate.Iterator[T], v T, ok bool) {
	u, nk := it.Next()
	if ok != nk {
		if ok {
			t.Fatal("Expected next")
		} else {
			t.Fatal("Expected done")
		}
	}

	if (interface{})(v) != (interface{})(u) {
		t.Fatalf("Unexpected value: %v != %v", v, u)
	}
}

func TestIterate(t *testing.T) {
	it := iterate.Values(12)()
	expect(t, it, 12, true)
	expect(t, it, 0, false)
}

func TestIterateSequence(t *testing.T) {
	it := iterate.Sequence(1, 1).Take(2)()
	expect(t, it, 1, true)
	expect(t, it, 2, true)
	expect(t, it, 0, false)
}

func TestIterateSum(t *testing.T) {
	const sum = 55
	it := iterate.Sequence(1, 1).Take(10)
	if s := iterate.Sum(it); s != sum {
		t.Fatalf("Expected %d, got %d", sum, s)
	}
}

func TestAll(t *testing.T) {
	a := iterate.Array(os.Environ())
	b := iterate.Transform(a, func(s string) KVP[string, string] {
		x := strings.SplitN(s, "=", 2)
		return KVP[string, string]{x[0], x[1]}
	})
	env := iterate.ToMap(b.Iterate())

	x := iterate.Map(env)
	y := x.Filter(func(kvp KVP[string, string]) bool { return strings.HasPrefix(kvp.Key, "GO") })
	l := iterate.Max(iterate.Transform(y, func(kvp KVP[string, string]) int { return len(kvp.Key) }))
	z := iterate.Transform(y, func(kvp KVP[string, string]) string {
		k := kvp.Key + ": "
		for len(k) < l+2 {
			k += " "
		}
		return k + kvp.Value
	})

	it := z
	fmt.Println("Count:", it.Iterate().Count())
	for v := range it.Iterate().ToChan(0) {
		fmt.Println(v)
	}
}